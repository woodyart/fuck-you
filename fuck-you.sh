#!/bin/bash
# I was inspired by this project - https://github.com/robotlolita/fuck-you
# and wrote bash version of it

function fcuk-flip() {
	case $1 in
		a) echo "ɐ"
			;;
		b) echo "q"
			;;
		c) echo "ɔ"
			;;
		d) echo "p"
			;;
		e) echo "ǝ"
			;;
		f) echo "ɟ"
			;;
		g) echo "ƃ"
			;;
		h) echo "ɥ"
			;;
		i) echo "ı"
			;;
		j) echo "ɾ"
			;;
		k) echo "ʞ"
			;;
		l) echo "ʃ"
			;;
		m) echo "ɯ"
			;;
		n) echo "u"
			;;
		p) echo "d"
			;;
		q) echo "b"
			;;
		r) echo "ɹ"
			;;
		t) echo "ʇ"
			;;
		u) echo "n"
			;;
		v) echo "ʌ"
			;;
		w) echo "ʍ"
			;;
		x) echo "x"
			;;
		y) echo "ʎ"
			;;
		*) echo "$1"
			;;
esac
}
function fcuk() {
	process=${*: -1:1}

	ssecorp=""
	while read i;
	do
		ssecorp+=$(fcuk-flip $i);
	done < <(echo $process | rev | grep -o .)

	killall $process 2>/dev/null
	[ $? -eq 0 ] \
		&& echo "(╯°□°）╯︵ $ssecorp" \
		|| echo "(:￣Д￣）. о О ( It's not very effective... )"
}

fcuk $@
